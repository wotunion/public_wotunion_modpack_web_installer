﻿////////////////////////////////////////////////////////////////////////
//wotunion modpack Web installer, types.iss, (c) 2016, http://goo.gl/JStiQ2 
////////////////////////////////////////////////////////////////////////
[Types]
Name: "Full"; Description: {#modpack} v.{code:GetActualVersions|modpack} ({code:GetActualVersions|modpack_Date}), {cm:for} WOT {code:GetActualVersions|for_WOT} - {cm:FullDescr}; 
Name: "Custom"; Description: {#modpack} v.{code:GetActualVersions|modpack} ({code:GetActualVersions|modpack_Date}), {cm:for} WOT {code:GetActualVersions|for_WOT} - {cm:CustomDescr}; Flags: iscustom;
