﻿////////////////////////////////////////////////////////////////////////
//wotunion modpack Web installer, lib.iss, (c) 2016, http://goo.gl/JStiQ2 
////////////////////////////////////////////////////////////////////////
#define WOT_RegKey_root "HKLM"
#define WOT_RegKey "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1"
#define WOT_RegKey_Name "InstallLocation"
#define WOT_RegKey2_root "HKCR"
#define WOT_RegKey2 ".wotreplay\shell\open\command"
#define WOT_RegKey2_Name ""
#define BackupFolder "w_backup_res_mods_original"

[Code]
var
 ReadyToInstall: Boolean;
 TerminateInstall: Boolean;
 Var_WOT_res_mods_Folder: string;
 Var_WOT_folder: string;
 InfoLabel: TLabel; 
 BitmapImage: TBitmapImage;
 WelcomePageID: Integer;
 UserLocalAppDataTemp: String;
 SetupDone: Boolean;
 StepBeforeUnpack: Boolean;
 StepAfterUnpack: Boolean;
 DownloadIndicator: TNewProgressBar;
 BackClicked: Boolean;
 Percent: Longint;
 CurrentDownloadTask: String;
 BeginTime: Longint;
 ElapsedTime: Longint;
 Bsec: Longint;
 Temp: Integer;
 TempFolder: String;
 IsComponentFixedArr: TArrayOfBoolean;
 MinimumInstallButton: TNewButton;
 BackupFolder: String;
 BackupFolderPathEdit: TCustomEdit;
 BackupFolderPathEditInfoLabel: TLabel;
 CreateBackupCheckBox: TNewCheckBox;
 DoNotCreateBackup: Boolean;
 DiskSpaceLabel: TLabel;
 Var_WOT_Version: String;

function IsAllowedDownloads():Boolean;
  begin
    if GetConfigInfo('IsAllowedDownloads', 'Status')='1' then
      begin
        Result:=True;
      end
      else
      begin
        Sleep(500);
        InfoLabel.Caption := ExpandConstant('{cm:DownloadIsNotAvailableInfoLabel_Msg}');
        WizardForm.Refresh;
        Sleep(1000);
        MsgBox(ExpandConstant('{cm:DownloadIsNotAvailable_Msg}') + #13#10#13#10 + ExpandConstant('{cm:DownloadIsNotAvailable_Text}') + #13#10#13#10 + GetConfigInfo('IsAllowedDownloads', ExpandConstant('{language}') + '.Message'), mbconfirmation, mb_OK); 
        Result:=False;
    end;
  end;

function Init(): Boolean;
  label exitsetup;
  label gotoend; 
    begin
      if CheckConfigInfo = False then goto exitsetup;
      Result := True;
      goto gotoend;
      exitsetup: Result:= False;
      gotoend:
    end;

//Парсинг значения ключа реестра по-умолчанию для типа файла .wotreplay, где содержится путь к установленному клиенту World of Tanks. Функция отдает путь к клиенту или '' в случае неудачи
//(Данный ключ ([HKEY_CLASSES_ROOT\.wotreplay\shell\open\command]) универсален как для Win 7, так и для Win XP и др. в отличие от [HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1])

function parseInstallstring():String;
  var
    WOTInstallPath: String;
    isInstalled: Boolean;
    Position: Integer;
      begin
        WOTInstallPath:='';
        isInstalled := RegQueryStringValue(HKCR, '{#WOT_RegKey2}', '{#WOT_RegKey2_Name}', WOTInstallPath);
          if (WOTInstallPath <> '') then 
            begin
              Position:=Pos('worldoftanks.exe', WOTInstallPath);
              Result:=Copy(WOTInstallPath, 2, (Pos('worldoftanks.exe', WOTInstallPath)-2));
            end
            else  
            begin
              Result:='';
          end; 
      end;

procedure BeforeUninstallDeleteTempFiles();
  begin
    DeleteFile(Var_WOT_res_mods_Folder+'0.9.13\scripts\client\mods\SessionStatCache.xml');
  end;  

procedure DeleteTestFiles();
  begin
    DeleteFile(Var_WOT_res_mods_Folder+'testfile');
    DeleteFile(Var_WOT_folder+'testfile'); 
  end;

procedure DeleteTestFolderAndRename();
  begin
    DelTree(Var_WOT_folder+'testfolder', True, False, False);
    RenameFile(Var_WOT_folder+'test_res_mods_original', Var_WOT_res_mods_Folder); 
  end;

function CheckNoErrors(): Boolean;
  begin
    if DirExists(Var_WOT_res_mods_Folder)=False then
      begin
        MsgBox(ExpandConstant('{cm:FolderNotFound}'), mbConfirmation, MB_OK);
        TerminateInstall:=True;
        WizardForm.Close;
        exit;
      end
      else
      begin
        if 
           ((SaveStringToFile(Var_WOT_folder+'testfile', 'test', False)=True)  and
           (CreateDir(Var_WOT_folder+'testfolder')=True) and 
           (SaveStringToFile(Var_WOT_res_mods_Folder+'testfile', 'test', False)=True) and                               
           (RenameFile(Var_WOT_res_mods_Folder, Var_WOT_folder+'test_res_mods_original')=True)) then 
              begin         
                DeleteTestFolderAndRename();
                DeleteTestFiles();       
                Result:=True; 
              end
              else
              begin        
                DeleteTestFolderAndRename();  
                DeleteTestFiles();                  
                Result:=False;
          end;
    end;
  end;        
    
function CheckModpackAlreadyInstalled(): Boolean;
  begin
    if ((DirExists('{#AppData}' + '\wotunion_modpack\uninstall')=False) and (DirExists(ExpandConstant('{localappdata}') + '\wotunion_modpack\uninstall')=False)) then
      begin
        Result:=True;
      end
      else
      begin
        Result:=False;
    end; 
  end;
   
function Backup(): Boolean;
  var ResultCode: Integer;
    begin
      If not DirExists(Var_WOT_Folder + 'wotunion_modpack_backups') then CreateDir(Var_WOT_Folder + 'wotunion_modpack_backups');
      ShellExec('', 'cmd.exe', '/c move "'+Var_WOT_Folder + 'res_mods'+'"' + ' ' + '"'+BackupFolder+'"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
      If ResultCode=0 then Result:=True else Result:=False;
    end;  

function BeforeInstallRenameOriginal(): Boolean;
  begin
    if RenameFile(Var_WOT_res_mods_Folder, Var_WOT_folder+'{#BackupFolder}')=False then
      begin
        if RenameFile(Var_WOT_res_mods_Folder, Var_WOT_folder+'{#BackupFolder}'+ '_' + GetDateTimeString('dd/mm/yyyy hh:nn:ss', '-', '-')) = True then Result:=False;
      end
      else
      begin
        Result:=True;
    end;
  end;  

procedure AfterUninstallRestoreOriginal();
  begin
    if DirExists(Var_WOT_res_mods_Folder)=False then
      begin
        if RenameFile(Var_WOT_folder+'{#BackupFolder}', Var_WOT_res_mods_Folder)=False then
          begin
            MsgBox(ExpandConstant('{cm:NotFound_w_backup_res_mods_original}'), mbconfirmation, Mb_OK);
            CreateDir(Var_WOT_res_mods_Folder);
            CreateDir(Var_WOT_res_mods_Folder + Var_WOT_Version);
        end;
      end
      else
      begin
       MsgBox(ExpandConstant('{cm:NotModpackFilesFound}'), mbconfirmation, Mb_OK);
    end;
  end; 
   
procedure HandleTerminateRetryCancelErrorWhileInstalling();
  begin
    if DirExists(Var_WOT_res_mods_Folder)=False then
      begin
        CreateDir(Var_WOT_res_mods_Folder);
        CreateDir(Var_WOT_res_mods_Folder + Var_WOT_Version);
    end;
  end;  

function NoBackup_Delete(): Boolean;
  begin
    If DoNotCreateBackup=True then
      begin
        If DelTree(Var_WOT_res_mods_Folder, True, True, True)<>True then
          begin
            MsgBox(ExpandConstant('{cm:DeleteCheckAccess}'), mbConfirmation, MB_OK);
            TerminateInstall:=True;
            WizardForm.Close;
            Result:=False;
            exit;
        end;
        HandleTerminateRetryCancelErrorWhileInstalling;
        Result:=True;
    end; 
  end;
    
function Get_WOT_InstallFolder():Boolean;  
  var 
    WOTInstallPath: String;
      begin
        WOTInstallPath:=parseInstallstring;
        if (WOTInstallPath <> '') then 
          begin
            Var_WOT_res_mods_Folder:=WOTInstallPath+'res_mods\';
            Var_WOT_folder:=WOTInstallPath;
            ////////////////////////////////////////////////////////////////////////////
            //For Test purposes
            ////////////////////////////////////////////////////////////////////////////
            //Var_WOT_res_mods_Folder:='d:\WOT\Tools\TEST IIS\WOF\res_mods\';
            //Var_WOT_folder:='d:\WOT\Tools\TEST IIS\WOF\';
            ////////////////////////////////////////////////////////////////////////////
            Result:=True;
          end
          else  
          begin
            MsgBox(ExpandConstant('{cm:WOT_NotFound}'), mbConfirmation, MB_OK);
            Result:=False;
        end; 
      end;

function Get_WOT_Version(S:String):String;
  begin
    Result:=GetConfigInfo('for_WOT', 'Version');
  end;


function Init2(): Boolean;
  label exitsetup;
  label gotoend; 
    var
      Stage1, Stage2, Stage3: boolean;
        begin
          Stage1:=False;
          Stage2:=False;
          Stage3:=False;
          if Get_WOT_InstallFolder=True then  Stage1:=True else goto exitsetup;
          if  CheckNoErrors=True then
            begin
              Stage2:=True;
            end
            else
            begin
              MsgBox(ExpandConstant('{cm:CheckAccess}'), mbConfirmation, MB_OK);
              Result:=False;
              TerminateInstall:=True;
              WizardForm.Close;
              exit;
          end;  
          Stage3:=True;
          if ((stage1=True) and (stage2=True) and (stage3=True))  then
            begin
              ReadyToInstall:=False;
              TerminateInstall:=False;
              SetupDone := False;
              StepAfterUnpack := False;
              BackupFolder:=Var_WOT_Folder + 'wotunion_modpack_backups' + '\' + 'res_mods' + '_' + GetDateTimeString('dd/mm/yyyy', '-', '-') + '_' +  GetDateTimeString('hh:nn:ss', '-', '-');
              Result := True;
            end
            else
            begin
              Result:=False;
          end;
          goto gotoend;
          exitsetup: Result:= False;
          gotoend:
        end;

function GetUserLocalAppDataTemp():String;
  begin
    Result:= ExpandConstant('{localappdata}') + '\Temp\';
  end;

function Form_CreatePage(PreviousPageId: Integer): Integer;
  var
    Page: TWizardPage;
      begin
        Page := CreateCustomPage(PreviousPageId, '', '');
        WelcomePageID := Page.ID;
        BitmapImage := TBitmapImage.Create(WizardForm);
        BitmapImage.Bitmap.LoadFromFile(ExpandConstant('{tmp}\wotunion_modpack.bmp'));
        BitmapImage.Top := 0;
        BitmapImage.Left := 0;
        BitmapImage.AutoSize := True;
        BitmapImage.Cursor := crHand;
        BitmapImage.Visible := False;
        BitmapImage.Parent := WizardForm.InnerPage;
        BitmapImage.Align:=alCLient;
        BitmapImage.Stretch:=True; 
        InfoLabel := TLabel.Create(WizardForm);
        with InfoLabel  do
          begin
            Parent := WizardForm.InnerPage;
            Visible:=False;
            Left := ScaleX(5);
            Top := ScaleY(292);
            Font.Color:= clWhite;
        end;
        Result := Page.ID;
      end;

Procedure ForumLinkOnClick (Sender: TObject); 
  var 
    ErrorCode: Integer; 
      Begin 
        ShellExec ('open', ExpandConstant('{cm:ForumLink}'),'','', SW_SHOWNORMAL, ewNoWait, ErrorCode); 
      end;

function SetComponentName(Name: String):Boolean;
  var l:Integer;
    begin
      l:=Length(IsComponentFixedArr);
      If (GetConfigInfo(Name, 'Enabled'))='1' then  
        begin
          SetArrayLength(IsComponentFixedArr, l+1);
          if GetConfigInfo(Name, 'Fixed')='1' then  IsComponentFixedArr[l]:=True  else  IsComponentFixedArr[l]:=False;
          Result:=True;
        end
        else 
        begin
          Result:=False;
      end;
    end;

procedure CheckComponentsEnabled();
  var i: Integer;
    begin
      for i:=0 to WizardForm.ComponentsList.Items.Count-1 do
        begin
          if IsComponentFixedArr[i]=True then 
            begin
              WizardForm.ComponentsList.Checked[i]:=IsComponentFixedArr[i];
              WizardForm.ComponentsList.ItemEnabled[i]:=False;
          end;
      end;
      WizardForm.ComponentsList.Refresh;
    end;

procedure MinimumInstallButtonOnClick(Sender: TObject);
  var i:Integer;
    begin 
      WizardForm.TypesCombo.ItemIndex:=1;
        for i:=0 to WizardForm.ComponentsList.Items.Count-1 do
          begin
            if WizardForm.ComponentsList.ItemEnabled[i]=False then WizardForm.ComponentsList.Checked[i]:=True else WizardForm.ComponentsList.Checked[i]:=False;
        end;
    end;

procedure InitializeWizard();
  var ForumLink: TNewStaticText;
    begin
      Form_CreatePage(wpwelcome);
      with WizardForm.WizardSmallBitmapImage do
        begin
          Left := ScaleX(0);
          Width := ScaleX(500);
          Height := ScaleY(58);
      end;
      with WizardForm.PageDescriptionLabel do
        begin
          Visible := False;
      end;
      with WizardForm.PageNameLabel do
        begin
          Visible := False;
      end;
      ForumLink := TNewStaticText.Create(WizardForm);
      ForumLink.Caption := ExpandConstant('{cm:SupportForum}');
      ForumLink.Parent := WizardForm;
      ForumLink.Left := ScaleX(16);
      ForumLink.Top := WizardForm.BackButton.Top + (WizardForm.BackButton.Height div 2) - (ForumLink.Height div 2);
      ForumLink.OnClick:=@ForumLinkOnClick;
      ForumLink.Cursor:= crHand;
      ForumLink.Font.Style:= ForumLink.Font.Style + [fsUnderline]; 
      ForumLink.Font.Color:= clBlue;
      DwinsHs_InitializeWizard(wpPreparing);
      DownloadIndicator := TNewProgressBar.Create(PageFromId(welcomepageid));
      DownloadIndicator.Left := ScaleX(0);
      DownloadIndicator.Top := ScaleY(308);
      DownloadIndicator.Width := ScaleX(500);
      DownloadIndicator.Height:= ScaleY(5);
      DownloadIndicator.Min := 0;
      DownloadIndicator.Parent := WizardForm;
      DownloadIndicator.Visible:= False;
      MinimumInstallButton := TNewButton.Create(WizardForm);
      MinimumInstallButton.Caption := ExpandConstant('{cm:MinimumInstallButtonCaption}');
      MinimumInstallButton.Parent := WizardForm;
      MinimumInstallButton.Left := WizardForm.NextButton.Left-(WizardForm.CancelButton.Left - WizardForm.NextButton.Left);
      MinimumInstallButton.Top := WizardForm.BackButton.Top;
      MinimumInstallButton.Height:=WizardForm.BackButton.Height;
      MinimumInstallButton.Width:=WizardForm.BackButton.Width;
      MinimumInstallButton.OnClick:=@MinimumInstallButtonOnClick;
      BackupFolderPathEditInfoLabel:= TLabel.Create(WizardForm);
      BackupFolderPathEditInfoLabel.Caption := ExpandConstant('{cm:BackupFolderPathEditInfoLabel}');
      BackupFolderPathEditInfoLabel.Left := WizardForm.DirEdit.Left + ScaleX(0);
      BackupFolderPathEditInfoLabel.Top :=  WizardForm.DirEdit.Top + WizardForm.DirEdit.Height + ScaleY(8);
      BackupFolderPathEditInfoLabel.Parent := WizardForm.SelectDirPage;
      BackupFolderPathEdit:= TCustomEdit.Create(WizardForm);
      BackupFolderPathEdit.Left := BackupFolderPathEditInfoLabel.Left + ScaleX(0);;
      BackupFolderPathEdit.Top :=  BackupFolderPathEditInfoLabel.Top + BackupFolderPathEditInfoLabel.Height + ScaleY(8);         
      BackupFolderPathEdit.Width:=WizardForm.DirEdit.Width; 
      BackupFolderPathEdit.Height:=WizardForm.DirEdit.Height; 
      BackupFolderPathEdit.Enabled:=False;
      BackupFolderPathEdit.Parent := WizardForm.SelectDirPage;
      CreateBackupCheckBox:= TNewCheckBox.Create(WizardForm);
      CreateBackupCheckBox.Left := BackupFolderPathEditInfoLabel.Left + ScaleX(0);;
      CreateBackupCheckBox.Top :=  BackupFolderPathEdit.Top + BackupFolderPathEdit.Height + ScaleY(8);         
      CreateBackupCheckBox.Width:=WizardForm.DirEdit.Width + ScaleX(28); 
      BackupFolderPathEdit.Height:=WizardForm.DirEdit.Height; 
      CreateBackupCheckBox.Parent := WizardForm.SelectDirPage;
      CreateBackupCheckBox.Caption:=ExpandConstant('{cm:DoNotCreateBackup}');
      CreateBackupCheckBox.Checked:=DoNotCreateBackup;
      CreateBackupCheckBox.Enabled:=False;
      DiskSpaceLabel:= TLabel.Create(WizardForm);
      DiskSpaceLabel.Caption := ExpandConstant('{cm:DiskSpaceFull}') + ' ' + GetConfigInfo('modpack', 'SizeFull') + ExpandConstant('{cm:DiskSpaceMinimum}') + ' ' + GetConfigInfo('modpack', 'SizeMin');
      DiskSpaceLabel.Left := WizardForm.ComponentsList.Left + ScaleX(0);;
      DiskSpaceLabel.Top :=  WizardForm.ComponentsList.Top + WizardForm.ComponentsList.Height + ScaleY(5);
      DiskSpaceLabel.Width:=WizardForm.ComponentsList.Width; 
      DiskSpaceLabel.WordWrap:=True;
      DiskSpaceLabel.Parent := WizardForm.SelectComponentsPage;
    end;

function GetActualVersions(componentName: String): String;
  begin
    case componentName of
      'modpack_Date':
        begin
          Result:=GetConfigInfo('modpack', 'Date');
        end
        else
        begin
          if  GetConfigInfo(componentName, 'Date')<>'' then Result:=GetConfigInfo(componentName, 'Version') + ' (' + GetConfigInfo(componentName, 'Date') + ')' else Result:=GetConfigInfo(componentName, 'Version');
        end;
    end;
  end;

function GetActualDescription(componentName: String): String;
  begin
    Result:=GetConfigInfo(componentName, ExpandConstant('{language}') + '.Description');
  end;

function GetComponentsURL(componentName: String): String;
  begin
    Result:=GetConfigInfo('modpack', 'Date');
  end;

function GetComponentsFilename(componentName: String): String;
  begin
    Result:=GetConfigInfo('modpack', 'Date');
  end;

function GetComponentSize(componentName: String): String;
  begin
    Result:=GetConfigInfo(ComponentName, 'Size');
  end;

function WOT_Folder(S:String): String;
  begin
    Result := Var_WOT_folder;       
  end;

function WOT_res_mods_Folder(S:String): String;
  begin
    Result := Var_WOT_res_mods_Folder;
  end;

procedure WriteReadme();
  var arr: TArrayOfString;
    i: Integer;
      begin
        SetArrayLength(arr, 16);
        arr[0]:='{#MyAppName}' + ' v.' + GetConfigInfo('modpack', 'Version') + ' (' + GetConfigInfo('modpack', 'Date') + ')';
        for i:=1 to 15 do
          begin
            arr[i]:=GetConfigInfo('wotunion_modpack_info', 'Line' + IntToStr(i));
          end; 
        SaveStringsToUTF8File(Var_WOT_res_mods_Folder + 'wotunion_modpack_info.txt', arr, False);
      end;


procedure DeleteTempFolder();
  begin
    DelTree(UserLocalAppDataTemp + 'wotunion_modpack_temp', True, True, True);
  end;

function CreateTempFolder(): Boolean;
  begin
    If CurrentDownloadTask = 'Updater' then
      begin
        if CreateDir(UserLocalAppDataTemp + 'wotunion_modpack_temp')=True then 
          begin
            Result:=True; 
          end
          else
          begin        
            MsgBox(ExpandConstant('{cm:CannotCreateTempFolder}'), mbconfirmation, mb_OK);   
            Result:=False;
        end;    
    end;
  end;

function OnRead(URL, Agent: AnsiString; Method: TReadMethod; Index, TotalSize, ReadSize, 
  CurrentSize: LongInt; var ReadStr: AnsiString): Boolean;
    begin
      WizardForm.Refresh;
      if Index = 0 then DownloadIndicator.Max := TotalSize;
      DownloadIndicator.Position := ReadSize; // Update the download progress indicator 
      If TotalSize>0 then Percent:=(((ReadSize/1024)*100) / (TotalSize/1024));
      if ReadSize = TotalSize then Percent := 100;
      ElapsedTime:=DecodeDateTimeString(GetDateTimeString('yyyy-mm-dd hh:nn:ss', '-', ':')) - BeginTime;
      if ElapsedTime > 0 then Bsec := (ReadSize) div ElapsedTime else  Bsec := 0;
      If ((Temp=5) or (ReadSize=TotalSize)) then
        begin
          If CurrentDownloadTask='Updater' then InfoLabel.Caption:=ExpandConstant('{cm:CurrentDownloadTaskUpdater}') + inttostr(Percent) + '% ' + '(' + FormatSize(TotalSize) + '), ' + FormatSpeed(Bsec);
          DownloadIndicator.Visible:=True;
          Temp:=Temp-5
      end;
          Temp:=Temp+1
          Result := True; // Continue to download
          Result := not BackClicked;
    end;


//Распаковываем во временную паку скачанные архивы с выбранными компонентами модпака;

function UnpackComponents():Boolean;
  var
    FilesFound, ResultCode: Integer;
    FindRec: TFindRec;
    CurrentArchivesFolder:  String;
      begin
        If StepBeforeUnpack <> True then CurrentArchivesFolder:=UserLocalAppDataTemp + TempFolder else  CurrentArchivesFolder:=ExpandConstant('{tmp}') + '\';
        DownloadIndicator.Visible:=False;
        WizardForm.Refresh;
        InfoLabel.Caption:=ExpandConstant('{cm:UnpackingArchives}');
        WizardForm.Refresh;
        Sleep(100);
        FilesFound := 0;
        if FindFirst(CurrentArchivesFolder + '*.7z', FindRec) then begin
          try
            repeat
              if FindRec.Attributes and FILE_ATTRIBUTE_DIRECTORY = 0 then
                If StepBeforeUnpack <> True then ExecAsOriginalUser(UserLocalAppDataTemp + TempFolder + '7zdec.exe', 'x ' + UserLocalAppDataTemp + TempFolder + FindRec.Name, '', SW_HIDE, ewWaitUntilTerminated, ResultCode) else ExecAsOriginalUser(ExpandConstant('{tmp}') + '\7zdec.exe', 'x ' + ExpandConstant('{tmp}') + '\' + FindRec.Name, '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
            until not FindNext(FindRec);
            finally
              FindClose(FindRec);
          end;
        end;
        WizardForm.Refresh;
        Result:=True;
        Sleep(100);
      end;

procedure DownloadUpdate(URL: String; PathAndFilename, Task: String);
  var Response: AnsiString;
    Size: LongInt;
    ResultCode: Integer;
    Code: Integer;    
      begin 
        WizardForm.NextButton.Enabled:=False;
        DownloadIndicator.Position := 0;  
        Temp:=5;
        BeginTime:=DecodeDateTimeString(GetDateTimeString('yyyy-mm-dd hh:nn:ss', '-', ':'));
        Code:=DwinsHs_ReadRemoteURL(URL, '{#UserAgent}', rmGet, Response, Size, pathandfilename , @OnRead);
        case Code of  
          READ_OK:
            begin
              UnpackComponents;
              InfoLabel.Caption:=ExpandConstant('{cm:Run}');
              WizardForm.Refresh;
              Sleep(500);
              If ExecAsOriginalUser(UserLocalAppDataTemp + TempFolder + (GetConfigInfo(CurrentDownloadTask, 'FilenameEXE')), + '-' + GetSHA1OfFile(UserLocalAppDataTemp + TempFolder + (GetConfigInfo(CurrentDownloadTask, 'FilenameEXE'))), '', SW_SHOWNORMAL, ewWaitUntilIdle, ResultCode) = True then
                begin
                  TerminateInstall:=True;
                  WizardForm.Close;
                end
                else
                begin 
                  InfoLabel.Caption:=ExpandConstant('{cm:Run}') + ExpandConstant('{cm:CannotLabel}');
              end;
          end;
          READ_ERROR_CANCELED:
            begin
              WizardForm.CancelButton.onclick(WizardForm.CancelButton);
            end
            else  
            begin
              DwinsHs_CancelDownload:= cdExit;
              InfoLabel.Caption:= ExpandConstant('{cm:CannotDownload}') + ' ' + GetDownloadStateText(Code);
              MsgBox(GetDownloadStateText(Code), mbconfirmation, mb_ok);
          end;
        end;  
        If DwinsHs_CancelDownload = cdExit then WizardForm.CancelButton.onclick(WizardForm.CancelButton);
      end;

function IsRunAllowedCheck():Boolean;
  var param: String;
    begin
      param:=GetCmdTail;
      If ((Pos(GetConfigInfo('Update', 'InstallerSHA-1'), param) = 0)) or ((ExpandConstant('{src}') + '\') <> (UserLocalAppDataTemp)) then
        begin
          MsgBox('Not Allowed '+param+ExpandConstant('{localappdata}')+ExpandConstant('{srcexe}') + UserLocalAppDataTemp + (ExtractFileName(ExpandConstant('{srcexe}'))), mbconfirmation, mb_OK);
          Result:=False;
        end
      else Result:=True
    end;  

function VerifyInstaller():Boolean;
  begin
    if ((GetSHA1OfFile(ExtractFileName(ExpandConstant('{srcexe}'))) = GetConfigInfo('Installer', 'SHA-1')) and (ExtractFileName(ExpandConstant('{srcexe}')) = GetConfigInfo('Installer', 'Filename'))) then 
      begin
        Result:=True
      end
      else
      begin
        Result:=False;
        MsgBox(ExpandConstant('{cm:InstallerNotVerified}') + #13#10#13#10 + GetConfigInfo('Installer', 'TrustedLinks'), mbconfirmation, mb_OK);
    end;
  end;