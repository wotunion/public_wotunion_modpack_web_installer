﻿////////////////////////////////////////////////////////////////////////
//wotunion modpack Web installer, config.iss, (c) 2016, http://goo.gl/JStiQ2 
////////////////////////////////////////////////////////////////////////
[Code] 
var
  DNSLocalCacheCleared:Boolean;
  Config: TArrayOfString;
  ConfigText: String;
 
function DownloadFile(const AURL: string; var AResponse: string): Boolean;
  var
    Code, CodeTemp: Integer; 
    Response, TempResponse: AnsiString;
    Size: LongInt;
    ResultCode: Integer;
      begin
        /////////////////////////////////////////////////////////////////////////////////////////////
        //Попытка тестового соединения с сервером обновлений + возможность очистки локального DNS кэша
        /////////////////////////////////////////////////////////////////////////////////////////////
        CodeTemp:= DwinsHs_ReadRemoteURL(ExpandConstant('{cm:CheckConfigInfoTest}'), '{#UserAgent}', rmGet, TempResponse, Size, '' , nil);
          If ((CodeTemp=CONNECT_ERROR_OPENSESSION) or (CodeTemp=CONNECT_ERROR_SENDREQUEST)) then 
            begin
              if MsgBox(ExpandConstant('{cm:CannotCheckConfigTry}') + #13#10#13#10 + GetDownloadStateText(CodeTemp) + #13#10#13#10 + ExpandConstant('{cm:RetryURL}'), mbConfirmation, MB_YESNO) = IDYES then
              ExecAsOriginalUser(ExpandConstant('ipconfig'), '/flushdns', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
              DNSLocalCacheCleared:=True;
              Sleep(100); 
              CodeTemp:= DwinsHs_ReadRemoteURL(AURL, '{#UserAgent}', rmGet, TempResponse, Size, '' , nil);
          end;
          TempResponse:='';           
          Result := False;
          Code:= DwinsHs_ReadRemoteURL(AURL, '{#UserAgent}', rmGet, Response, Size, '' , nil);
          case Code of  
            READ_OK:
              begin
                if AURL = (ExpandConstant('{cm:CheckConfigInfo}')) then ConfigText:=Response;
                Result:=True
              end;
            else  
              begin
                Aresponse:=GetDownloadStateText(Code);
              end;
          end;
      end;
 
procedure ConvertUTF8CodesToSymbols;
  var i, ii:Integer;
    UTF8_Symbol, UTF8_Code:  TArrayOfString;
      begin
        LoadStringsFromFile(ExpandConstant('{tmp}') + '\UTF8\UTF-8_Code.txt', UTF8_Code);
        LoadStringsFromFile(ExpandConstant('{tmp}') + '\UTF8\UTF-8_Symbol.txt', UTF8_Symbol);
          for i:=0 to Length(Config)-1 do
            begin
              if ((pos(ExpandConstant('{language}') + '.Description', Config[i]) <> 0) or (pos(ExpandConstant('{language}') + '.Message', Config[i]) <> 0))  then 
                begin
                  for ii:=0 to Length(UTF8_Code)-1 do
                    StringChangeEx(Config[i], UTF8_Code[ii], UTF8_Symbol[ii], False);
              end;              
          end; 
      end;

function StringsSplitToArray(const S: string): Boolean;
  var i, k, Len, Count: Integer;
    begin
      Len := Length(S);
      Count := 0;
      k := 1;
      for i := 1 to Len do
        begin
          if ((S[i] = #13) and (s[i+1]=#10)) then
            begin
              SetArrayLength(Config, Count+1);
              Config[Count]:=Copy(s, k, i-k);
              Inc(Count);
              k := i + 2;
          end; 
      end; 
      SetArrayLength(Config, Count+1);
      Config[Count]:=Copy(s, k, Len-k+1);
      ConvertUTF8CodesToSymbols;
      Result:=True; 
    end;

function CheckConfigInfo():Boolean;
  var response:String;
    begin
      if DownloadFile((ExpandConstant('{cm:CheckConfigInfo}')), response) <> True then
        begin
          MsgBox((ExpandConstant('{cm:CannotCheckConfig}')) + #13#10#13#10 + response, mbconfirmation, mb_OK);
          Result:=False;
        end
        else
        begin
          StringsSplitToArray(ConfigText);
          Result:=True;
      end;
    end;

//Парсинг конфигурации. Происходит в памяти.

function GetConfigInfo(Section: String; Name: String): String; 
  var   
    i, ii: integer; 
      begin
        Result:='';
        for i:=0 to GetArrayLength(Config)-1 do
          begin        
            if Pos('[' + Section + ']', Config[i])<>0  then  
              begin 
                ii:=i;
                for ii:=i+1 to GetArrayLength(Config)-1 do                    
                  begin
                    if Pos('[', Config[ii])<>0 then exit;   
                    if Pos(Name+'=', Config[ii])<>0  then
                      begin
                        Result:=Copy(Config[ii], (Pos('=', Config[ii])+1), (Length(Config[ii])-Length(Name)) );
                        exit;
                    end;              
                end;
            end;
        end;  
      end;

     
        

  

