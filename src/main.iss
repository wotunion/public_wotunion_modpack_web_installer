﻿////////////////////////////////////////////////////////////////////////
//wotunion modpack Web installer, main.iss, (c) 2016, http://goo.gl/JStiQ2 
////////////////////////////////////////////////////////////////////////
#define MyAppName "wotunion modpack"
#define modpack "wotunion modpack"
#define wotunion_modpack_Web_installer "wotunion modpack Web Installer"
#define wotunion_modpack_Web_installer_filename "wotunion_modpack_Web_installer"
#define UserAgent "wotunion_modpack_Web_installer"
#define installer_versionInfo "6"
#define MyAppPublisher "wotunion"
#define MyAppURL "http://goo.gl/JStiQ2"
#define AppData GetEnv('AppData');
#define BackupFolder "w_backup_res_mods_original"
#define BackupFolder "w_backup_res_mods_original"
////////////////////////////////////////////////////////////////////////
[Setup]
AppId={{18511B57-52D6-4562-AA88-BA1BE63C500E}
AppName={#MyAppName}
AppVersion={#installer_versionInfo}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={code:WOT_res_mods_Folder}
DisableDirPage=no
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputBaseFilename={#wotunion_modpack_Web_installer_filename}_v.{#installer_versionInfo}
Compression=lzma
SolidCompression=yes
DisableReadyPage=False
DisableFinishedPage=yes
UninstallFilesDir={localappdata}\wotunion_modpack\uninstall
DirExistsWarning=no
WizardSmallImageFile=..\img\wotunion_modpack_small.bmp
PrivilegesRequired=none
LanguageDetectionMethod=locale
DisableReadyMemo=False
CreateUninstallRegKey=no
VersionInfoVersion={#installer_versionInfo}
VersionInfoCompany=wotunion
VersionInfoDescription={#MyAppName} Web installer
VersionInfoTextVersion={#MyAppName} Web installer
VersionInfoCopyright=wotunion
VersionInfoProductName=wotunion modpack
VersionInfoProductVersion=
VersionInfoProductTextVersion=
DisableWelcomePage=yes
ShowLanguageDialog=yes
OutputDir=..\Output

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "ukrainian"; MessagesFile: "compiler:Languages\Ukrainian.isl"
Name: "english"; MessagesFile: "compiler:Default.isl"

[Code] 
#include "files.iss"
#include "components.iss"
#include "types.iss"
#include "messages.iss"
#define DwinsHs_Use_Predefined_Downloading_WizardPage
#define DwinsHs_Data_Buffer_Length 16384
#define DwinsHs_Auto_Continue
#define DwinsHs_Disable_Default_CustomMessages
#include "..\external_lib\dwinshs.iss"
#include "config.iss"
#include "lib.iss"

function BackButtonClick(CurPageID: Integer): Boolean;
  begin
    Result := True;
    DwinsHs_BackButtonClick(CurPageID);
  end;

Procedure WizardFormOnclick(Sender: TObject); 
  begin
    MsgBox('Click', mbconfirmation, mb_OK);
  end; 
 
procedure CurPageChanged(CurPageID: Integer);
  label exitsetup;
  label gotoend; 
  Var VersionInstallerActual, VersionmodpackActual, VersionInstallerCurrent, temp, ErrorCode: Integer;
  BackupFolderText: String;
    begin       
      InfoLabel.Visible := CurPageID = WelcomePageID; 
      BitmapImage.Visible := CurPageID = WelcomePageID;
      WizardForm.Bevel1.Visible := CurPageID <> WelcomePageID;
      WizardForm.MainPanel.Visible := CurPageID <> WelcomePageID;
      WizardForm.InnerNotebook.Visible := CurPageID <> WelcomePageID;
      DownloadIndicator.Visible:= CurPageID = WelcomePageID;
      MinimumInstallButton.Visible:=CurPageID=wpSelectComponents;
      WizardForm.BackButton.Visible:=CurPageID<>WelcomePageID;
      WizardForm.show;
      WizardForm.Refresh;
        if CurPageID = wpSelectDir then 
          begin
            WizardForm.DirEdit.Enabled := False;
            WizardForm.DirEdit.Text:=Var_WOT_res_mods_Folder;
            WizardForm.DirBrowseButton.Enabled:=False;
            WizardForm.SelectDirBitmapImage.Visible:=False;
            WizardForm.BackButton.Visible:=False;
            BackupFolderText:=BackupFolder; 
            StringChangeEx(BackupFolderText, Var_WOT_Folder, '\', False);
            BackupFolderPathEdit.Text :=   BackupFolderText;
            BackupFolderPathEdit.Hint:=BackupFolder;
            if GetConfigInfo('modpack', 'IsAllowedToDisableBackups')='1' then CreateBackupCheckBox.Enabled:=True;
        end;
        if CurPageID = wpSelectComponents then 
          begin
            WizardForm.BackButton.Visible:=False;
            WizardForm.ComponentsDiskSpaceLabel.Visible := False;
            CheckComponentsEnabled;
            CheckComponentsEnabled;
        end;
        DwinsHs_CurPageChanged(CurPageID, nil, nil);
        if CurPageID=welcomepageid then   
          begin    
            WizardForm.NextButton.Enabled := False;
            if IsAllowedDownloads = False then goto exitsetup;
            Sleep(500);
            VersionInstallerActual:=StrToIntDef((GetConfigInfo('Installer' , 'Version')), temp); 
            VersionmodpackActual:=StrToIntDef((GetConfigInfo('modpack' , 'Version')), temp); 
            VersionInstallerCurrent:=StrToIntDef('{#installer_versionInfo}', temp);
            WizardForm.Caption:=WizardForm.Caption + ' ' + GetConfigInfo('for_WOT', 'Version');  
              If VersionInstallerActual > VersionInstallerCurrent then 
                begin
                  CurrentDownloadTask:='Updater';
                  InfoLabel.Caption:=ExpandConstant('{cm:UpdateFound}') + ' {#wotunion_modpack_Web_installer}' + ' v.' + IntToStr(VersionInstallerActual); 
                  WizardForm.Refresh;
                  WizardForm.NextButton.Enabled:=False; 
                  Sleep(1500);                    
                    if MsgBox(ExpandConstant('{cm:NewVersionFound}') + ' {#wotunion_modpack_Web_installer}' + ':' + #13#10#13#10 + 'v.' + IntToStr(VersionInstallerActual) + ' ' + #13#10#13#10 + ExpandConstant('{cm:UpdateInstaller}') + #13#10#13#10 + ExpandConstant('{cm:GoToLink}'), mbConfirmation, MB_YESNO) = IDYES then
                      begin         
                        ShellExec ('open', ExpandConstant('{cm:ForumLink}'),'','', SW_SHOWNORMAL, ewNoWait, ErrorCode); 
                        Sleep(1000);
                        TerminateInstall:=True;
                        WizardForm.Close;
                      end
                      else  
                      begin
                        TerminateInstall:=True;
                        WizardForm.Close;
                    end;
                end
                else
                begin
                  CurrentDownloadTask:='Installer';  
                    if FileCopy(ExpandConstant('{tmp}\7z_LZMA_SDK\7zdec.exe'), ExpandConstant('{tmp}') + '\7zdec.exe', False) <> True then
                      begin
                        MsgBox(ExpandConstant('{cm:UnpackerError}'), mbconfirmation, mb_OK);       
                        goto exitsetup;
                    end; 
                    If init2=True then 
                      begin
                        InfoLabel.Caption:=ExpandConstant('{cm:AllIsReadyForInstall}');
                        WizardForm.NextButton.Enabled := True;
                    end;
              end;
        end;
      goto gotoend;
      exitsetup: 
      TerminateInstall:=True;
      WizardForm.Close;
      gotoend:
    end;

function InitializeSetup(): Boolean;
  label exitsetup;
  label gotoend; 
    begin
      DoNotCreateBackup:=False; 
      UserLocalAppDataTemp:='wotunion_modpack_folder';
      UserLocalAppDataTemp:=GetUserLocalAppDataTemp;
      DeleteTempFolder;
      if MsgBox(ExpandConstant('{cm:LoadConfigQuestion}'), mbConfirmation, MB_YESNO) <> IDYES then goto exitsetup;
      // Распаковка во временную папку картинки с фоном инсталлятора и распаковщика 7-Zip from LZMA SDK (Software Development Kit) (2015-12-31  15.14 LZMA SDK (C, C++, C#, Java) (только командная строка, для последующего использования при распаковке архивов компонентов модпака, скачанных с Web-сервера)
      ExtractTemporaryFile(ExtractFileName('img\wotunion_modpack.bmp'));
      ExtractTemporaryFiles(ExtractFileName('*7z*.*'));
      ExtractTemporaryFiles(ExtractFileName('*.txt')); 
      if Init() = False then goto exitsetup;
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      //Uncomment on release, enabling installer .exe verification 
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
      if VerifyInstaller <> True then goto exitsetup;
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      Var_WOT_Version:=Get_WOT_Version('');
      Sleep(100);
      Sleep(100); 
      Result := True;
      goto gotoend;
      exitsetup: Result:= False;
      gotoend:
    end;

function NextButtonClick(CurPageID: Integer): Boolean;
  begin
    if CurPageID=wpSelectDir then 
      begin
        DoNotCreateBackup:=CreateBackupCheckBox.Checked;
    end;
    Result:=True;
    DwinsHs_NextButtonClick(CurPageID, Result);
  end;

procedure CancelButtonClick(CurPageID: Integer; var Cancel, Confirm: Boolean);
  begin
    if CurPageID=WelcomePageID then 
      begin
        if DwinsHs_CancelDownload = cdExit then Confirm := False;
    end;
    if TerminateInstall=True then Confirm := False; 
    DwinsHs_CancelButtonClick(CurPageID, Cancel, Confirm);
  end;

function InitializeUninstall(): Boolean;
 begin
  Get_WOT_InstallFolder;
   if  CheckNoErrors=True then
    begin
     Result := True;
    end
    else
    begin
     MsgBox(ExpandConstant('{cm:CheckAccessUninstall}'), mbConfirmation, MB_OK);
     Result:=False;
     exit;
    end;  
  Result := True;
 end;

procedure DeinitializeSetup();
var ResultCode: Integer;
  begin                           
    DeleteTempFolder;
    if DNSLocalCacheCleared=True then ExecAsOriginalUser(ExpandConstant('ipconfig'), '/flushdns', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
    If ((SetupDone <> True) and (StepAfterUnpack = True) and (StepBeforeUnpack = True)) then  
      begin  
        MsgBox('Handling', mbconfirmation, mb_OK);
        HandleTerminateRetryCancelErrorWhileInstalling();
    end;
  end;

procedure CurStepChanged(CurStep: TSetupStep);
  begin
    case CurStep of    
      ssInstall:
        begin
          WizardForm.BackButton.Visible:=False;
          NoBackup_Delete();
          If DoNotCreateBackup<>True then
            begin  
              If Backup<>True then
                begin
                  MsgBox(ExpandConstant('{cm:BackupCheckAccess}'), mbConfirmation, MB_OK);
                  TerminateInstall:=True;
                  WizardForm.Close;
                  exit;
              end;
          end;
          WizardForm.StatusLabel.Caption:=ExpandConstant('{cm:UnpackingFiles}');
          StepBeforeUnpack:=True;  
          UnpackComponents;
          StepAfterUnpack:=True;
        end;
      ssDone:
        begin
         SetupDone:=True;
         WriteReadme();
         MsgBox(ExpandConstant('{cm:InstallComplete}'), mbconfirmation, mb_OK);
        end;
    end;
  end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
  begin
    case CurUninstallStep of
      usUninstall:
        begin
          if Get_WOT_InstallFolder=True then BeforeUninstallDeleteTempFiles();
        end;
      usPostUninstall:
        begin         
          if Get_WOT_InstallFolder=True then AfterUninstallRestoreOriginal();
        end;
    end; 
  end;

function ShouldSkipPage(CurPageID: Integer): Boolean;
  begin
    Result := False;
    DwinsHs_ShouldSkipPage(CurPageID, Result);
  end;

