﻿////////////////////////////////////////////////////////////////////////
//wotunion modpack Web installer, components.iss, (c) 2016, http://goo.gl/JStiQ2 
////////////////////////////////////////////////////////////////////////
[Components]
;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
;Base Components
;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
;0
Name: "XVM"; Description: {cm:XVMDescr} {code:GetActualVersions|XVM} {code:GetComponentSize|XVM}; Types: Full; Check: SetComponentName('XVM');   

;1
Name: "XVM\wotunion_XVM_config"; Description: {cm:wotunion_XVM_configDescr}{code:GetActualVersions|XVM\wotunion_XVM_config} ; Types: Full custom; Flags: ; Check: SetComponentName('XVM\wotunion_XVM_config'); 

;2
Name: "Master_XH"; Description: {cm:Master_XHDescr}; Types: Full;  Check: SetComponentName('Master_XH');  

;3
Name: "Audio"; Description: {cm:SixthSenseSoundDescr}; Types: Full;    Check: SetComponentName('SixthSenseSound'); 

;4
Name: "Audio\CopyAudio"; Description: {cm:CopyAudioDescr}; Types: Full; Flags: exclusive; Check: SetComponentName('Audio\CopyAudio'); 

;5
Name: "DamageIndicator"; Description: {cm:DamageIndicatorDescr}; Types: Full; Check: SetComponentName('DamageIndicator');  

;6
Name: "DamageLog"; Description: {cm:DamageLogDescr}; Types: Full;  Check: SetComponentName('DamageLog'); 

;7
Name: "DamageLog\CopyAudio"; Description: {cm:CopyAudioDescr}; Types: Full;   Flags: exclusive; Check: SetComponentName('DamageLog\CopyAudio')

;8
Name: "MCM"; Description: {cm:MCMDescr}; Types: Full; Check: SetComponentName('MCM'); 

;9
Name: "SessionStat"; Description: {cm:SessionStatDescr}; Types: Full; Check: SetComponentName('SessionStat'); 

;10
Name: "SixthSenseIcon"; Description: {cm:SixthSenseIconDescr}; Types: Full; Check: SetComponentName('SixthSenseIcon');

;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
;Custom Components (cc(n) - Simple Component, ccwa(n) - Component with copy "audio\res", cc(n)\inh(n) - Component inherits from cc(n))
;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Name: "cc1"; Description: {code:GetActualDescription|cc1}; Types: Full; Check: SetComponentName('cc1');
      Name: "cc1\inh1"; Description: {code:GetActualDescription|cc1\inh1}; Types: Full; Check: SetComponentName('cc1\inh1');  
      Name: "cc1\inh2"; Description: {code:GetActualDescription|cc1\inh2}; Types: Full; Check: SetComponentName('cc1\inh2');
      Name: "cc1\inh3"; Description: {code:GetActualDescription|cc1\inh3}; Types: Full; Check: SetComponentName('cc1\inh3');
      Name: "cc1\inh4"; Description: {code:GetActualDescription|cc1\inh4}; Types: Full; Check: SetComponentName('cc1\inh4');
      Name: "cc1\inh5"; Description: {code:GetActualDescription|cc1\inh5}; Types: Full; Check: SetComponentName('cc1\inh5');
;
Name: "cc2"; Description: {code:GetActualDescription|cc2}; Types: Full; Check: SetComponentName('cc2');  
Name: "cc3"; Description: {code:GetActualDescription|cc3}; Types: Full; Check: SetComponentName('cc3');  
Name: "cc4"; Description: {code:GetActualDescription|cc4}; Types: Full; Check: SetComponentName('cc4');  
Name: "cc5"; Description: {code:GetActualDescription|cc5}; Types: Full; Check: SetComponentName('cc5');
;
Name: "cc6"; Description: {code:GetActualDescription|cc6}; Types: Full; Check: SetComponentName('cc6');
      Name: "cc6\inh1"; Description: {code:GetActualDescription|cc6\inh1}; Types: Full; Check: SetComponentName('cc6\inh1');  
      Name: "cc6\inh2"; Description: {code:GetActualDescription|cc6\inh2}; Types: Full; Check: SetComponentName('cc6\inh2');
;  
Name: "cc7"; Description: {code:GetActualDescription|cc7}; Types: Full; Check: SetComponentName('cc7');
      Name: "cc7\inh1"; Description: {code:GetActualDescription|cc7\inh1}; Types: Full; Check: SetComponentName('cc7\inh1');  
      Name: "cc7\inh2"; Description: {code:GetActualDescription|cc7\inh2}; Types: Full; Check: SetComponentName('cc7\inh2');
;            
Name: "cc8"; Description: {code:GetActualDescription|cc8}; Types: Full; Check: SetComponentName('cc8');
      Name: "cc8\inh1"; Description: {code:GetActualDescription|cc8\inh1}; Types: Full; Check: SetComponentName('cc8\inh1');  
      Name: "cc8\inh2"; Description: {code:GetActualDescription|cc8\inh2}; Types: Full; Check: SetComponentName('cc8\inh2');
;            
Name: "cc9"; Description: {code:GetActualDescription|cc9}; Types: Full; Check: SetComponentName('cc9');
      Name: "cc9\inh1"; Description: {code:GetActualDescription|cc9\inh1}; Types: Full; Check: SetComponentName('cc9\inh1');  
      Name: "cc9\inh2"; Description: {code:GetActualDescription|cc9\inh2}; Types: Full; Check: SetComponentName('cc9\inh2');
;        
Name: "cc10"; Description: {code:GetActualDescription|cc10}; Types: Full; Check: SetComponentName('cc10');  
      Name: "cc10\inh1"; Description: {code:GetActualDescription|cc10\inh1}; Types: Full; Check: SetComponentName('cc10\inh1');  
      Name: "cc10\inh2"; Description: {code:GetActualDescription|cc10\inh2}; Types: Full; Check: SetComponentName('cc10\inh2');
;
Name: "cc11"; Description: {code:GetActualDescription|cc11}; Types: Full; Check: SetComponentName('cc11');
      Name: "cc11\inh1"; Description: {code:GetActualDescription|cc11\inh1}; Types: Full; Check: SetComponentName('cc11\inh1');  
      Name: "cc11\inh2"; Description: {code:GetActualDescription|cc11\inh2}; Types: Full; Check: SetComponentName('cc11\inh2');

;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
Name: "ccwa1"; Description: {code:GetActualDescription|ccwa1}; Types: Full; Check: SetComponentName('ccwa1');
      Name: "ccwa1\inh1"; Description: {code:GetActualDescription|ccwa1\inh1}; Types: Full; Flags: exclusive; Check: SetComponentName('ccwa1\inh1'); 
;        
Name: "ccwa2"; Description: {code:GetActualDescription|ccwa2}; Types: Full; Check: SetComponentName('ccwa2');
      Name: "ccwa2\inh1"; Description: {code:GetActualDescription|ccwa2\inh1}; Types: Full; Flags: exclusive;  Check: SetComponentName('ccwa2\inh1');
;          
Name: "ccwa3"; Description: {code:GetActualDescription|ccwa3}; Types: Full; Check: SetComponentName('ccwa3');  
      Name: "ccwa3\inh1"; Description: {code:GetActualDescription|ccwa3\inh1}; Types: Full; Flags: exclusive; Check: SetComponentName('ccwa3\inh1');
;        
Name: "ccwa4"; Description: {code:GetActualDescription|ccwa4}; Types: Full; Check: SetComponentName('ccwa4');  
      Name: "ccwa4\inh1"; Description: {code:GetActualDescription|ccwa4\inh1}; Types: Full; Flags: exclusive; Check: SetComponentName('ccwa4_inh1');
;        
Name: "ccwa5"; Description: {code:GetActualDescription|ccwa5}; Types: Full; Check: SetComponentName('ccwa5');
      Name: "ccwa5\inh1"; Description: {code:GetActualDescription|ccwa5\inh1}; Types: Full; Flags: exclusive; Check: SetComponentName('ccwa5\inh1');  

;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                                                      
Name: "ccr1"; Description: {code:GetActualDescription|ccr1}; Types: Full; Check: SetComponentName('ccr1');
      Name: "ccr1\inh1"; Description: {code:GetActualDescription|ccr1\inh1}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr1\inh1'); 
      Name: "ccr1\inh2"; Description: {code:GetActualDescription|ccr1\inh2}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr1\inh2'); 
      Name: "ccr1\inh3"; Description: {code:GetActualDescription|ccr1\inh3}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr1\inh3'); 
      Name: "ccr1\inh4"; Description: {code:GetActualDescription|ccr1\inh4}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr1\inh4'); 
      Name: "ccr1\inh5"; Description: {code:GetActualDescription|ccr1\inh5}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr1\inh5'); 
;
Name: "ccr2"; Description: {code:GetActualDescription|ccr2}; Types: Full; Check: SetComponentName('ccr2');
      Name: "ccr2\inh1"; Description: {code:GetActualDescription|ccr2\inh1}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr2\inh1'); 
      Name: "ccr2\inh2"; Description: {code:GetActualDescription|ccr2\inh2}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr2\inh2'); 
      Name: "ccr2\inh3"; Description: {code:GetActualDescription|ccr2\inh3}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr2\inh3'); 
      Name: "ccr2\inh4"; Description: {code:GetActualDescription|ccr2\inh4}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr2\inh4'); 
      Name: "ccr2\inh5"; Description: {code:GetActualDescription|ccr2\inh5}; Types: Full; Flags: exclusive; Check: SetComponentName('ccr2\inh5'); 
;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
 