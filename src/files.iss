﻿////////////////////////////////////////////////////////////////////////
//wotunion modpack Web installer, files.iss, (c) 2016, http://goo.gl/JStiQ2 
////////////////////////////////////////////////////////////////////////
[Files]
; Temporary 7-Zip from LZMA SDK (Software Development Kit) (2015-12-31	15.14	LZMA SDK (C, C++, C#, Java) + Unicode  helpers (.txt)
Source: "..\helpers\*"; DestDir: "{tmp}"; Flags: ignoreversion recursesubdirs createallsubdirs;

; Temporary bitmap image file
Source: "..\img\wotunion_modpack.bmp"; DestDir: "{tmp}"; Flags: ignoreversion recursesubdirs createallsubdirs; 

;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
;Base Files
;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

; XVM
Source: "{tmp}\XVM\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: XVM; Check: DwinsHs_Check(ExpandConstant('{tmp}\XVM.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/XVM/XVM.7z', '{#UserAgent}', 'Get', 0);

; CopyResAudio
Source: "{code:WOT_Folder}\res\audio\*"; DestDir: "{app}\{code:Get_WOT_Version}\audio"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: Audio DamageLog ccwa1 ccwa2 ccwa3 ccwa4 ccwa5; 

; SixthSenseSound
Source: "{tmp}\SixthSenseSound\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: Audio;Check: DwinsHs_Check(ExpandConstant('{tmp}\SixthSenseSound.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/SixthSenseSound/SixthSenseSound.7z', '{#UserAgent}', 'Get',  0); 

; DamageIndicator
Source: "{tmp}\DamageIndicator\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: DamageIndicator;Check: DwinsHs_Check(ExpandConstant('{tmp}\DamageIndicator.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/DamageIndicator/DamageIndicator.7z', '{#UserAgent}', 'Get', 0);

; DamageLog
Source: "{tmp}\DamageLog\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: DamageLog;Check: DwinsHs_Check(ExpandConstant('{tmp}\DamageLog.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/DamageLog/DamageLog.7z', '{#UserAgent}', 'Get', 0);

; Master_XH
Source: "{tmp}\Master_XH\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: Master_XH; Check: DwinsHs_Check(ExpandConstant('{tmp}\Master_XH.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/Master_XH/Master_XH.7z', '{#UserAgent}', 'Get', 0);

; MCM
Source: "{tmp}\MCM\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: MCM; Check: DwinsHs_Check(ExpandConstant('{tmp}\MCM.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/MCM/MCM.7z', '{#UserAgent}', 'Get', 0);     

; SessionStat
Source: "{tmp}\SessionStat\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: SessionStat; Check: DwinsHs_Check(ExpandConstant('{tmp}\SessionStat.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/SessionStat/SessionStat.7z', '{#UserAgent}', 'Get', 0);

; SixthSense Icon                   
Source: "{tmp}\SixthSenseIcon\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: SixthSenseIcon; Check: DwinsHs_Check(ExpandConstant('{tmp}\SixthSenseIcon.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/SixthSenseIcon/SixthSenseIcon.7z', '{#UserAgent}', 'Get', 0);

; wotunion XVM config
Source: "{tmp}\wotunion_XVM_config\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: XVM\wotunion_XVM_config; Check: DwinsHs_Check(ExpandConstant('{tmp}\wotunion_XVM_config.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/wotunion_XVM_config/wotunion_XVM_config.7z', '{#UserAgent}', 'Get', 0);
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
;Custom Files (cc(n) - Simple Component, ccwa(n) - Component with copy "audio\res", ccr(n) - Component with inherited exclusive components, cc*(n)\inh(n) - Component inherits from cc(n))
;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Source: "{tmp}\cc1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc1/cc1.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\cc1_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc1\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc1_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc1_inh1/cc1_inh1.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\cc1_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc1\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc1_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc1_inh2/cc1_inh2.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\cc1_inh3\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc1\inh3;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc1_inh3.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc1_inh3/cc1_inh3.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\cc1_inh4\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc1\inh4;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc1_inh4.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc1_inh4/cc1_inh4.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\cc1_inh5\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc1\inh5;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc1_inh5.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc1_inh5/cc1_inh5.7z', '{#UserAgent}', 'Get',  0); 

Source: "{tmp}\cc2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc2/cc2.7z', '{#UserAgent}', 'Get',  0);
Source: "{tmp}\cc3\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc3;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc3.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc3/cc3.7z', '{#UserAgent}', 'Get',  0);
Source: "{tmp}\cc4\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc4;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc4.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc4/cc4.7z', '{#UserAgent}', 'Get',  0);
Source: "{tmp}\cc5\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc5;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc5.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc5/cc5.7z', '{#UserAgent}', 'Get',  0);
;
Source: "{tmp}\cc6\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc6;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc6.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc6/cc6.7z', '{#UserAgent}', 'Get',  0);
        Source: "{tmp}\cc6_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc6\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc6_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc6_inh1/cc6_inh1.7z', '{#UserAgent}', 'Get',  0); 
         Source: "{tmp}\cc6_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc6\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc6_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc6_inh2/cc6_inh2.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\cc7\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc7;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc7.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc7/cc7.7z', '{#UserAgent}', 'Get',  0);
        Source: "{tmp}\cc7_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc7\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc7_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc7_inh1/cc7_inh1.7z', '{#UserAgent}', 'Get',  0); 
         Source: "{tmp}\cc7_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc7\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc7_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc7_inh2/cc7_inh2.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\cc8\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc8;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc8.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc8/cc8.7z', '{#UserAgent}', 'Get',  0);
        Source: "{tmp}\cc8_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc8\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc8_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc8_inh1/cc8_inh1.7z', '{#UserAgent}', 'Get',  0); 
         Source: "{tmp}\cc8_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc8\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc8_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc8_inh2/cc8_inh2.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\cc9\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc9;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc9.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc9/cc9.7z', '{#UserAgent}', 'Get',  0);
        Source: "{tmp}\cc9_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc9\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc9_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc9_inh1/cc9_inh1.7z', '{#UserAgent}', 'Get',  0); 
         Source: "{tmp}\cc9_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc9\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc9_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc9_inh2/cc9_inh2.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\cc10\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc10;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc10.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc10/cc10.7z', '{#UserAgent}', 'Get',  0);
        Source: "{tmp}\cc10_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc10\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc10_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc10_inh1/cc10_inh1.7z', '{#UserAgent}', 'Get',  0); 
         Source: "{tmp}\cc10_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc10\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc10_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc10_inh2/cc10_inh2.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\cc11\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc11;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc11.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc1/cc11.7z', '{#UserAgent}', 'Get',  0);
        Source: "{tmp}\cc11_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc11\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc11_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc11_inh1/cc11_inh1.7z', '{#UserAgent}', 'Get',  0); 
         Source: "{tmp}\cc11_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: cc11\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\cc11_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/cc11_inh2/cc11_inh2.7z', '{#UserAgent}', 'Get',  0); 
;;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Source: "{tmp}\ccwa1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa1/ccwa1.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccwa1_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa1\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa1_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa1_inh1/ccwa1_inh1.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\ccwa2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa2;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa2/ccwa2.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccwa2_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa2\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa2_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa2_inh1/ccwa2_inh1.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\ccwa3\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa3;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa3.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa3/ccwa3.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccwa3_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa3\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa3_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa3_inh1/ccwa3_inh1.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\ccwa4\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa4;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa4.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa4/ccwa4.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccwa4_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa4\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa4_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa4_inh1/ccwa4_inh1.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\ccwa5\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa5;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa5.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa5/ccwa5.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccwa5_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccwa5\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccwa5_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccwa5_inh1/ccwa5_inh1.7z', '{#UserAgent}', 'Get',  0); 

;;/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Source: "{tmp}\ccr1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr1/ccr1.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr1_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr1\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr1_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr1_inh1/ccr1_inh1.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr1_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr1\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr1_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr1_inh2/ccr1_inh2.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr1_inh3\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr1\inh3;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr1_inh3.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr1_inh3/ccr1_inh3.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr1_inh4\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr1\inh4;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr1_inh4.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr1_inh4/ccr1_inh4.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr1_inh5\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr1\inh5;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr1_inh5.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr1_inh5/ccr1_inh5.7z', '{#UserAgent}', 'Get',  0); 
;
Source: "{tmp}\ccr2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr2;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr2/ccr2.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr2_inh1\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr2\inh1;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr2_inh1.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr2_inh1/ccr2_inh1.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr2_inh2\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr2\inh2;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr2_inh2.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr2_inh2/ccr2_inh2.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr2_inh3\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr2\inh3;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr2_inh3.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr2_inh3/ccr2_inh3.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr2_inh4\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr2\inh4;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr2_inh4.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr2_inh4/ccr2_inh4.7z', '{#UserAgent}', 'Get',  0); 
        Source: "{tmp}\ccr2_inh5\*"; DestDir: "{app}"; Flags: external ignoreversion recursesubdirs createallsubdirs; Components: ccr2\inh5;Check: DwinsHs_Check(ExpandConstant('{tmp}\ccr2_inh5.7z'), 'http://wotunion.xyz/mods/download/modpack_Components/ccr2_inh5/ccr2_inh5.7z', '{#UserAgent}', 'Get',  0); 
