[wotunion_modpack_Config]
Version=0.9.14

[for_WOT]
Version=0.9.14

[Installer]
Version=6
Date=09.03.2016
URL=http://goo.gl/NLZbwo
Filename=wotunion_modpack_Web_installer_v.6.exe
SHA-256=
SHA-1=83af98e64a6824b5d7607fa49eaa5562f534c95f
ForumLink=
TrustedLinks=koreanrandom.com - http://goo.gl/ZXPKhP 

[modpack]
Description=
Version=304
Date=13.03.2016
SizeFull=~18 MB
SizeMin=~12 MB
IsAllowedToDisableBackups=1

[IsAllowedDownloads] /Messages in UTF-8
Status=1
ukrainian.Message=\u041F\u0456\u0434\u0433\u043E\u0442\u043E\u0432\u043A\u0430\u0020\u043C\u043E\u0434\u0456\u0432\u0020\u0434\u043B\u044F wotunion modpack v.304
english.Message=Preparing mods for wotunion modpack v.304
russian.Message=\u041F\u043E\u0434\u0433\u043E\u0442\u043E\u0432\u043A\u0430\u0020\u043C\u043E\u0434\u043E\u0432\u0020\u0434\u043B\u044F wotunion modpack v.304

[wotunion_modpack_info]
Line1=
Line2=Created with Inno Script Studio v.2.2.2.32 {20151020-065430} {https://www.kymoto.org/products/inno-script-studio} and compiled by Inno Setup Compiler v.5.5.8 {u} {http://www.jrsoftware.org/isinfo.php};
Line3=Installer uses DwinsHs {Downloader for Inno Setup} Version: 1.1.2.118 {For Inno Setup 5.x} {http://www.han-soft.com/dwinshs.php}; 
Line4=Installer uses 7-zip archive unpacker .7z Any / x86 / x64 LZMA SDK: {C, C++, C#, Java} {7zdec.exe} {http://www.7-zip.org/download.html};
Line5=
Line6=More info at koreanrandom.com {http://goo.gl/NLZbwo}
Line7=
Line8=
Line9=
Line10=
Line11=
Line12=
Line13=
Line14=
Line15=

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Components begin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[XVM]
SiteDescr=XVM: eXtended Visualization Mod
Version=6.2.1
Date=
Size=
URL=
Filename=
Enabled=1
Fixed=1

[SixthSenseSound]
Size=
URL=
Filename=
Enabled=0
Fixed=0

[Audio\CopyAudio]
Enabled=0
Fixed=0

[DamageIndicator]
SiteDescr=Маркер направления атаки
Size=
URL=
Filename=
Enabled=1
Fixed=0

[DamageLog]
Size=
URL=
Filename=
Enabled=0
Fixed=0

[DamageLog\CopyAudio]
Enabled=0
Fixed=0

[Master_XH]
SiteDescr=Іконки танков в ушах от Master_XH
Size=
URL=
Filename=
Enabled=1
Fixed=1

[MCM]
SiteDescr=Цветные сообщения ЯсенКрасен
Size=
URL=
Filename=
Enabled=1
Fixed=0

[SessionStat]
SiteDescr=Статистика за сессию + конфигурация
Size=
URL=
Filename=
Enabled=1
Fixed=0

[SixthSenseIcon]
SiteDescr=Иконка лампы перка шестое чувство
Size=
URL=
Filename=
Enabled=1
Fixed=0

[XVM\wotunion_XVM_config]
SiteDescr=Конфигурация XVM от wotunion
english.Description=DamageLog from GambitER
ukrainian.Description=DamageLog \u0432\u0456\u0434 GambitER
russian.Description=DamageLog \u043E\u0442 GambitER
ConfigName=wotunion
Version=2.1
Date=11.03.2016
Size=
URL=
Filename=
Enabled=1
Fixed=1

[cc1]
SiteDescr=DamageLog от GambitER
english.Description=DamageLog from GambitER
ukrainian.Description=DamageLog \u0432\u0456\u0434 GambitER
russian.Description=DamageLog \u043E\u0442 GambitER
Version=0.3.1
Date=10.03.2016
Enabled=1
Fixed=0

[cc2]
SiteDescr=Звуковой отсчет лампы шестого чувства
english.Description=Sound countdown for sixth sense perk lamp
ukrainian.Description=\u0417\u0432\u0443\u043A\u043E\u0432\u0438\u0439\u0020\u0432\u0456\u0434\u043B\u0456\u043A\u0020\u043B\u0430\u043C\u043F\u0438\u0020\u0448\u043E\u0441\u0442\u043E\u0433\u043E\u0020\u0447\u0443\u0442\u0442\u044F
russian.Description=\u0417\u0432\u0443\u043A\u043E\u0432\u043E\u0439\u0020\u043E\u0442\u0441\u0447\u0435\u0442\u0020\u043B\u0430\u043C\u043F\u044B\u0020\u0448\u0435\u0441\u0442\u043E\u0433\u043E\u0020\u0447\u0443\u0432\u0441\u0442\u0432\u0430
Version=
Date=
Enabled=1
Fixed=0

[wotunion_XVM_config]
Enabled=1



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Components end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

